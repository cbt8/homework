
def fib_iterative(n):
    a, b = 0, 1
    while n > 0:
        a, b = b, a + b
        n -= 1
    return a
for i in range(0,12):
    print( fib_iterative(i))
